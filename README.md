# docker-dev

Docker images for portable development environments

## Images:

* ubuntu-dev-base: Base image with tmux, neovim with plugins, fzf, zsh with powerlevel9k, ag.
* python-dev: Development environment with python2.7 and python3.6 virtualenvs
* docker-dev: Development environment with go and various build tools
* purescript-dev: Development environment with nodejs and purescript
