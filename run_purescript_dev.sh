docker run -e DISPLAY -it --name "purescript_dev" -h "purescript_dev" -v /tmp/.X11-unix:/tmp/.X11-unix \
-v ~/.gitconfig:/home/devbox/.gitconfig -v ~/shared:/home/devbox/shared \
-v $SSH_AUTH_SOCK:/ssh-agent --env SSH_AUTH_SOCK=/ssh-agent \
hoborg/purescript-dev $@
