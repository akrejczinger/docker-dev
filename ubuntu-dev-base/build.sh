#!/usr/bin/env bash

# If one command exits with an error, stop the script immediately
set -e

# Print every line executed
set -x

# Install fzf
git clone https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --all

# Install newest silversearcher
git clone https://github.com/ggreer/the_silver_searcher /tmp/the_silver_searcher
pushd /tmp/the_silver_searcher
./build.sh
sudo make install
popd
sudo rm -rf /tmp/the_silver_searcher

# Homesick repositories
# TODO: can we use ssh git access here as well?
homesick clone https://github.com/akrejczinger/zshrc.git
homesick symlink zshrc --force

homesick clone https://github.com/akrejczinger/dev-scripts.git
homesick symlink dev-scripts --force

homesick clone https://github.com/akrejczinger/dotvim.git
homesick symlink dotvim --force

# Mostly unused, but the lesskey file is still useful
homesick clone https://github.com/akrejczinger/dev-dotfiles.git
homesick symlink dev-dotfiles

# Install antibody
curl -sL https://git.io/antibody | bash -s
# Install antibody plugins
source <(antibody init) && source ~/.zshrc && antibody update

# the ~/.cache directory cannot be accessed for some reason, breaking vim
sudo rm -rf ~/.cache
mkdir ~/.cache

# Install neovim plugins
nvim -c "PlugInstall" || true # ignore errors
