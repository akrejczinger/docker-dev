#!/usr/bin/env zsh

# If one command exits with an error, stop the script immediately
set -e

# Virtualenvwrapper initialisation
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/bin/virtualenvwrapper.sh

mkvirtualenv py2.7 -r ~/requirements-2.7.txt -p /usr/bin/python2.7 || return 0
mkvirtualenv py3.6 -r ~/requirements-3.6.txt -p /usr/bin/python3.6 || return 0
